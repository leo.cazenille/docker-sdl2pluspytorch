FROM ubuntu:18.04
MAINTAINER leo.cazenille@gmail.com


# Update system
RUN \
    DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get upgrade -y && \
    echo "deb http://ppa.launchpad.net/ubuntu-toolchain-r/ppa/ubuntu bionic main" >> /etc/apt/sources.list.d/toolchain.list && \
    apt-get install -yq python3.7 python3-pip python3.7-dev python3-yaml git gosu rsync cmake && \
    update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1 && \
    update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2 && \
    rm -rf /var/lib/apt/lists/* && \
    rm -fr /etc/apt/sources.list.d/toolchain.list


# Install python dependencies
RUN pip3 --no-cache-dir install numpy pandas scoop Cython


# TODO caffe-cpu gcc-8 ? torch ? SDL dependencies
RUN \
    DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get upgrade -y && \
#    apt-get install -yq wget unzip caffe-cpu libeigen3-dev libsdl2-2.0 libsdl2-dev libsdl2-image-dev libboost-all-dev automake && \
    apt-get install -yq wget unzip libeigen3-dev libboost-all-dev automake mercurial cmake libsdl2-2.0 libsdl2-dev libsdl2-image-dev && \
    apt-get purge -yq libsdl2-2.0 libsdl2-dev libsdl2-image-dev && \
    rm -rf /var/lib/apt/lists/*

RUN \
    hg clone https://hg.libsdl.org/SDL && cd SDL && mkdir build && cd build && cmake -DVIDEO_OFFSCREEN=ON -DCMAKE_INSTALL_PREFIX=/usr .. && make -j 20 && make install && cd ../..

RUN \
    hg clone http://hg.libsdl.org/SDL_image && cd SDL_image && autoreconf -f -i; ./configure --prefix=/usr && make -j 20 && make install && cd ..

RUN \
    wget https://download.pytorch.org/libtorch/cpu/libtorch-cxx11-abi-shared-with-deps-1.3.1%2Bcpu.zip && \
    mkdir -p /opt/torch && \
    unzip -d /opt/torch libtorch-cxx11-abi-shared-with-deps-1.3.1+cpu.zip && \
    rm -fr libtorch-cxx11-abi-shared-with-deps-1.3.1+cpu.zip


# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
